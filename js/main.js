/* Javascript */

// For your reference:

// Develop an HTML5 Snake Game with the below specifications:

// The snake can be controlled by arrows.
// Snake food will be placed randomly on screen.
// Snake food has a random timeout between 4 to 10 seconds
// The snake grows longer by 1 segment after eating the food.
// Eating 1 piece of food earns 1 point.
// If the snake hits a boundary, the size of the game area reduces by 113 pixels and the direction of the snake reverses.
// The game is over when the snake contacts itself.
// Dimensions of food and of one segment of the snake are both 16px X 16px.
// The starting game area is 800px X 800px.
// A "Play Again" button should appear once the game is over.
// In the subsequent games, if the score is greater than the previous score, change the color of the score.

// To get bonus points: implement a special food with the following properties:

// Eating special food gains 9 points and elongates the snake's length by 2 units.
// It can be of any shape other than a square.
// It must fit inside 28px X 28px box
// It is a random color.
// Its timeout interval lies between 1 and 5 seconds.

const game = (function (canvas_id) {
  // game rules
  const pixel_size = 16;
  const field_size = 800;
  const shrink_by = 113;
  const food_life = { min: 4000, max: 10000 };

  const top_left = { x: 0, y: 0 };
  //   const control_keys = ["ArrowRight", "ArrowLeft"];
  const directions = { right: 0, up: 90, left: 180, down: 270 };

  // parameters
  // the snake moves one square every refresh_interval milliseconds
  const refresh_interval = 100;
  let refreshId;
  const field_start = { x: 8, y: 8 };
  let canvas;
  let ctx;
  let game_state;

  let turns = {
    left: "ArrowLeft",
    right: "ArrowRight",
  };

  let input = [];

  // start game with snake going up
  let direction = 90;

  // field object is responsible for:
  // - maintaining the playing field size and bounds
  // - detecting collision of its opposite edges
  let field = (() => {
    let bounds, field_size, pixel_size, top_left;

    function initialize(args) {
      // { field_size, pixel_size, top_left } = args;
      field_size = args.field_size;
      pixel_size = args.pixel_size;
      top_left = args.top_left;

      bounds = {
        x: {
          min: top_left.x + pixel_size,
          max: top_left.x + pixel_size + field_size,
        },
        y: {
          min: top_left.y + pixel_size,
          max: top_left.y + pixel_size + field_size,
        },
      };
    }

    // adjust the size of the field by edge and amount specified
    // if opposite edges come together, throw an error to end the game
    function adjustSize(args) {
      let { top, right, bottom, left } = args || {};

      top = top || 0;
      right = right || 0;
      bottom = bottom || 0;
      left = left || 0;

      bounds.x.min += left;
      bounds.x.max += right;
      bounds.y.min += top;
      bounds.y.max += bottom;

      if (bounds.x.min >= bounds.x.max || bounds.y.min >= bounds.y.max) {
        throw new Error("Field area shrunk to 0");
      }
    }

    function draw(args) {
      let { ctx, field_color, border_color } = args;

      ctx.fillStyle = field_color;
      ctx.fillRect(0, 0, field_size + 2 * pixel_size, field_size + 2 * pixel_size);

      ctx.lineWidth = pixel_size;
      ctx.strokeStyle = border_color;
      ctx.strokeRect(
        bounds.x.min - pixel_size / 2,
        bounds.y.min - pixel_size / 2,
        bounds.x.max - bounds.x.min + pixel_size,
        bounds.y.max - bounds.y.min + pixel_size
      );
    }

    // on collision, shrink the field from the opposite side
    function collisionHandler(args) {
      let { direction } = args;

      switch (direction) {
        case directions.right:
          adjustSize({ left: shrink_by });
          break;
        case directions.down:
          adjustSize({ top: shrink_by });
          break;
        case directions.left:
          adjustSize({ right: -shrink_by });
          break;
        case directions.up:
          adjustSize({ bottom: -shrink_by });
          break;
        default:
          console.log("Shouldn't see this!");
          break;
      }
    }

    function getBounds() {
      return bounds;
    }

    return {
      initialize: initialize,
      adjustSize: adjustSize,
      draw: draw,
      collisionHandler: collisionHandler,
      getBounds: getBounds,
    };
  })();

  // snake object is responsible for:
  // - initializing the snake
  // - keeping the state of the snake - position of head, length and position of tail
  // - detecting snake head collision with its tail
  let snake = (function () {
    let direction;
    let position;
    // segments[0] is the head
    let segments;
    let length;
    let initialLength;
    let bounds;
    let collisionHandler;
    let previous_turn;

    // function initialize({ direction, position, length }) {
    function initialize(args) {
      direction = args.direction || 0;
      position = args.position || { x: 400, y: 400 };
      length = args.length || 2;
      initialLength = length;
      getBounds = args.getBounds;
      collisionHandler = args.collisionHandler;

      segments = [];
      for (i = 0; i < length; i++) {
        segments.push({ x: position.x, y: position.y });
      }
    }

    // direction in degrees, 0 is along the +x axis, 90 along +y, etc.
    function turn(turn) {
      switch (turn) {
        case turns.left:
          direction += 90;
          break;
        case turns.right:
          direction -= 90;
          break;
        default:
          // do nothing
          break;
      }

      // keep direction between 0 and 359
      if (direction < 0) {
        direction += 360;
      }

      if (direction >= 360) {
        direction %= 360;
      }
    }

    function checkForCollisionWithBorders() {
      let collision = false;
      let bounds = getBounds();

      switch (direction) {
        case directions.right:
          if (segments[0].x + pixel_size > bounds.x.max) {
            collision = true;
          }
          break;
        case directions.up:
          if (segments[0].y - pixel_size < bounds.y.min) {
            collision = true;
          }
          break;
        case directions.left:
          if (segments[0].x - pixel_size < bounds.x.min) {
            collision = true;
          }
          break;
        case directions.down:
          if (segments[0].y + pixel_size > bounds.y.max) {
            collision = true;
          }
          break;
        default:
          console.log("Unexpected direction");
          break;
      }

      return collision;
    }

    function checkForCollisionWithSelf() {
      let collision = false;
      // where the snake head is moving to
      let x, y;

      x = segments[0].x;
      y = segments[0].y;

      switch (direction) {
        case directions.right:
          x += pixel_size;
          break;
        case directions.up:
          y -= pixel_size;
          break;
        case directions.left:
          x -= pixel_size;
          break;
        case directions.down:
          y += pixel_size;
          break;
        default:
          console.log("Unexpected direction");
          break;
      }

      // length - 2 because the tail is moving out of this pixel
      // i > 3 because the head cannot collide with segments 2, 3, and 4
      for (i = segments.length - 2; i > 3; i--) {
        if (x === segments[i].x && y === segments[i].y) {
          throw Error("Snake collided with itself");
        }
      }
    }

    function checkForFood() {
      let food_position = food.getPosition();
      if (food_position) {
        if (
          Math.abs(segments[0].x - food_position.x) < pixel_size &&
          Math.abs(segments[0].y - food_position.y) < pixel_size
        ) {
          // let the food know it's been eaten
          food.eat();
          // grow the snake
          segments.push({ x: segments[segments.length - 1].x, y: segments[segments.length - 1].y });
          length++;
          console.log("Current score: ", length - initialLength);
        }
      }
    }

    function move() {
      checkForFood();
      checkForCollisionWithSelf();

      for (i = segments.length - 1; i > 0; i--) {
        segments[i].x = segments[i - 1].x;
        segments[i].y = segments[i - 1].y;
      }

      if (input.length > 0) {
        let current_turn = input.shift();
        turn(current_turn);
        previous_turn = current_turn;
      }

      if (checkForCollisionWithBorders()) {
        // let the game field react
        collisionHandler({ direction: direction });

        // reverse direction on collision
        // favor turning in direction opposite the previous turn (to lower the chances of ending the game)
        let consecutive_turns = turns.left;
        if (previous_turn) {
          if (previous_turn === turns.left) {
            consecutive_turns = turns.right;
          }
        }

        turn(consecutive_turns);
        input = [consecutive_turns];
      }

      switch (direction) {
        case directions.right:
          segments[0].x += pixel_size;
          break;
        case directions.up:
          segments[0].y -= pixel_size;
          break;
        case directions.left:
          segments[0].x -= pixel_size;
          break;
        case directions.down:
          segments[0].y += pixel_size;
          break;
        default:
          console.log("Unexpected direction");
          break;
      }
    }

    function drawSegment(args) {
      let { ctx, segment, color } = args;
      ctx.fillStyle = color;
      ctx.fillRect(segment.x - pixel_size / 2, segment.y - pixel_size / 2, pixel_size, pixel_size);
    }

    function draw(args) {
      let { ctx, color } = args;

      for (i = 0; i < length; i++) {
        drawSegment({ ctx: ctx, segment: segments[i], color: color });
      }
    }

    return {
      initialize: initialize,
      //   turn: turn,
      move: move,
      draw: draw,
    };
  })();

  // food object is responsible for:
  // - generating food at random position that will randomly expire within the defined period
  // - upon food expiring or being eaten, generate the next one
  let food = (function () {
    let bite, x, y, after, expire;

    function generate() {
      let bounds = getBounds();
      x = bounds.x.min + pixel_size / 2 + Math.random() * (bounds.x.max - bounds.x.min - pixel_size / 2);
      y = bounds.y.min + pixel_size / 2 + Math.random() * (bounds.y.max - bounds.y.min - pixel_size / 2);
      after = food_life.min + Math.random(food_life.max - food_life.min);

      expire = window.setTimeout(reset, after);
    }

    function reset() {
      window.clearTimeout(expire);
      generate();
    }

    function eaten() {
      reset();
    }

    function draw(args) {
      let { ctx, color } = args;

      ctx.fillStyle = color;
      ctx.fillRect(x - pixel_size / 2, y - pixel_size / 2, pixel_size, pixel_size);
    }

    function initialize() {
      generate();
    }

    function getPosition() {
      return { x, y };
    }

    return {
      initialize: initialize,
      draw: draw,
      getPosition: getPosition,
      eat: eaten,
    };
  })();

  const initial_state = {
    snake: snake,
    snake_direction: null,
    field_start: { x: field_start.x, y: field_start.y },
    field_width: field_size,
    field_height: field_size,
    food: {},
  };

  function drawGame(ctx, game_state) {
    field.draw({ ctx: ctx, field_color: "lightblue", border_color: "hotpink" });
    snake.draw({ ctx: ctx, color: "green" });
    food.draw({ ctx: ctx, color: "red" });
  }

  function refreshScreen() {
    try {
      snake.move();
      drawGame(ctx, game_state);
    } catch (e) {
      console.log("Ending Game. ", e);
      endGame();
    }
  }

  // will need to reset state at end of game
  function init() {
    canvas = document.getElementById(canvas_id);
    ctx = canvas.getContext("2d");

    field.initialize({ field_size, pixel_size, top_left });
    snake.initialize({
      direction: directions.up,
      position: { x: Math.floor(field_size / 2), y: Math.floor(field_size / 2) },
      length: 2,
      getBounds: field.getBounds,
      collisionHandler: field.collisionHandler,
    });
    food.initialize();
    return ctx;
  }

  // enqueue key inputs
  function handleKey(e) {
    if (e.code === turns.left || e.code === turns.right) {
      input.push(e.code);
      e.preventDefault();
    } else if (e.code === "Space") {
      startGame();
    } else if (e.code === "KeyR") {
      resetGame();
    }
  }

  function setupGame() {
    game_state = Object.assign({}, initial_state);
    ctx = init();

    document.addEventListener("keydown", handleKey);
    refreshScreen();
  }

  function startGame() {
    if (!refreshId) {
      refreshId = window.setInterval(refreshScreen, refresh_interval);
    }
  }

  function endGame() {
    window.clearInterval(refreshId);
    refreshId = null;
  }

  function resetGame() {
    input = [];
    window.clearInterval(refreshId);
    refreshId = null;
    game_state = Object.assign({}, initial_state);
    ctx = init();

    refreshScreen();
  }

  return {
    setup: setupGame,
    end: endGame,
  };
})("game");

game.setup();
